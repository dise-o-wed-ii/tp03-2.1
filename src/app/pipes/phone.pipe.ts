import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phone' // Nombre del filtro de tubería que se utilizará en las plantillas HTML
})
export class PhonePipe implements PipeTransform {

  // Implementación del método `transform` requerido por la interfaz PipeTransform
  transform(value: any, args?: any): any {
    return '+59-' + value; // Agrega el prefijo "+59-" al valor proporcionado
  }
}
