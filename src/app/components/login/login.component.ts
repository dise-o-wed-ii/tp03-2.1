/*
 * Este componente maneja el inicio de sesión de los usuarios.
 */
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router';
import { ValidationService } from '../../services/config/config.service';
import { UserService } from '../../services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { routerTransition } from '../../services/config/config.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;

	constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService, private toastr: ToastrService) {
		// Inicializa el formulario de inicio de sesión con validadores para el correo electrónico y la contraseña.
		this.loginForm = this.formBuilder.group({
			email: ['', [Validators.required, ValidationService.emailValidator]],
			password: ['', [Validators.required, ValidationService.passwordValidator]]
		});
	}

	// Comprueba si el usuario ya ha iniciado sesión
	ngOnInit() {
		if (localStorage.getItem('userData')) {
			// Redirige al usuario a la página principal si ya ha iniciado sesión.
			this.router.navigate(['/']);
		}
	}

	// Inicia el proceso de inicio de sesión
	doLogin() {
		// Llama al servicio de usuario para realizar el inicio de sesión y recibe una respuesta.
		let login = this.userService.doLogin(this.loginForm.value);
		// Maneja la respuesta del inicio de sesión.
		this.success(login);
	}

	// Función que se ejecuta cuando el inicio de sesión es exitoso
	success(data: any) {
		if (data.code == 200) {
			// Almacena los datos del usuario en el almacenamiento local y redirige al usuario a la página principal.
			localStorage.setItem('userData', JSON.stringify(data.data));
			this.router.navigate(['/']);
			// Muestra un mensaje de éxito utilizando Toastr.
			this.toastr.success('Éxito', "Sesión Exitosa");
		} else {
			// Muestra un mensaje de error si las credenciales son inválidas.
			this.toastr.error('Fallido', "Credenciales Inválidas");
		}
	}
}

/**
 * Creado por: Sangwin Gawande (https://sangw.in)
 * Este componente maneja el inicio de sesión de los usuarios.
 */
