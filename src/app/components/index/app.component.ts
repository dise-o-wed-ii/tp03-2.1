import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {
	title = 'Registro de Estudiantes';

	// Agregar algunos estudiantes para la lista inicial
	studentsList = [
		{
			id: 1,
			dni: 98335261,
			nombre: "Sangwin",
			apellido: "Gawande",
			correo: "sangwin@yopmail.com",
			carrera: "Derecho",
			telefono: 9503733178
			
		},
		{
			id: 2,
			dni: 9293847,
			nombre: "Oman",
			apellido: "Umir",
			correo: "oman@yopmail.com",
			carrera: "Comercio Exterior",
			telefono: 8574889658
		
		},
		{
			id: 3,
			dni: 91174523,
			nombre: "Tina",
			apellido: "Dillon",
			correo: "tina@yopmail.com",
			carrera: "Comunicacion Social",
			telefono: 7485889658
			
		},
		{
			id: 4,
			dni: 62638989,
			nombre: "John",
			apellido: "Doe",
			correo: "john@yopmail.com",
			carrera: "Ingenieria en Sistemas",
			telefono: 9685589748
			
		},
		{
			id: 5,
			dni: 98382924,
			nombre: "Peter",
			apellido: "Parker",
			correo: "peter@yopmail.com",
			carrera: "Ingenieria Comercial",
			telefono: 8595856547
			
		}
	];

	constructor() {
		// Guardar los estudiantes en el almacenamiento local (localStorage)
		localStorage.setItem('students', JSON.stringify(this.studentsList));
	}
}
