/**
 * Creado por: Sangwin Gawande (https://sangw.in)
 * Este archivo fue creado por Sangwin Gawande, y puedes encontrar más información en su sitio web.
 */

import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

// Componentes
import { StudentListComponent } from '../student/list/student-list.component';
import { StudentDetailsComponent } from '../student/details/student-details.component';
import { StudentAddComponent } from '../student/add/student-add.component';

// Servicios
import { routerTransition } from '../../services/config/config.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})

export class HomeComponent implements OnInit {
	active: string;

	constructor(private router: Router, private toastr: ToastrService) {
		this.active = '';
		// Detectar cambios en la ruta para el menú lateral activo
		this.router.events.subscribe((val) => {
			this.routeChanged(val);
		});
	}

	ngOnInit() {
	}

	// Detectar cambios en la ruta para el menú lateral activo
	routeChanged(val: any) {
		this.active = val.url;
	}

	// Cerrar sesión del usuario
	logOut() {
		this.toastr.success('Éxito', "Cierre de sesión exitoso");
		localStorage.removeItem('userData');
		this.router.navigate(['/login']);
	}
}

// Definir y exportar rutas secundarias de HomeComponent
export const homeChildRoutes: Routes = [
	{
		path: '',
		component: StudentListComponent
	},
	{
		path: 'add',
		component: StudentAddComponent
	},
	{
		path: 'update/:id',
		component: StudentAddComponent
	},
	{
		path: 'detail/:id',
		component: StudentDetailsComponent
	}
];

