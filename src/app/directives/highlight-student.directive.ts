

import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlightStudent]' // Selector para usar la directiva en la plantilla HTML
})
export class HighlightStudentDirective {

	constructor(private el: ElementRef) {
	}

	// Cuando el mouse entra en el elemento
	@HostListener('mouseenter') onMouseEnter() {
		this.highlight('gray'); // Resaltar el elemento cambiando el fondo a gris
	}

	// Cuando el mouse sale del elemento
	@HostListener('mouseleave') onMouseLeave() {
		this.highlight(''); // Eliminar el resaltado al restaurar el fondo a su estado original
	}

	// Función privada para cambiar el color de fondo del elemento
	private highlight(color: string) {
		this.el.nativeElement.style.backgroundColor = color;
	}
}


