/**
 * Creado por: Sangwin Gawande (https://sangw.in)
 */

import { Injectable } from '@angular/core';

@Injectable()
export class StudentService {

  constructor() { }

  // Obtener la lista de todos los estudiantes a través de una API o almacenamiento de datos
  getAllStudents() {
    let studentList: any;
    if (localStorage.getItem('students') && localStorage.getItem('students') !== '') {
      studentList = {
        code: 200,
        message: "Lista de estudiantes obtenida exitosamente",
        data: JSON.parse(localStorage.getItem('students') || '')
      };
    } else {
      studentList = {
        code: 200,
        message: "Lista de estudiantes obtenida exitosamente",
        data: JSON.parse(localStorage.getItem('students') || '')
      };
    }
    return studentList;
  }

  // Registrar un estudiante en la lista
doRegisterStudent(data: any, index: number) {
  let studentList = JSON.parse(localStorage.getItem('students') || '');
  let returnData;
  
  if (index !== null) {
    // Esto es una actualización, no se mostrará el mensaje en este caso
    studentList[index] = data;
    localStorage.setItem('students', JSON.stringify(studentList));
    returnData = {
      code: 200,
      message: "Estudiante actualizado exitosamente",
      data: JSON.parse(localStorage.getItem('students') || '')
    };
  } else {
    // Esto es una adición, aquí verificarás si el correo ya está en uso
    let emailInUse = studentList.some((student: { correo: any; }) => student.correo === data.correo);
    
    if (emailInUse) {
      returnData = {
        code: 503,
        message: "Dirección de correo electrónico ya en uso",
        data: null
      };
    } else {
      studentList.unshift(data);
      localStorage.setItem('students', JSON.stringify(studentList));
      returnData = {
        code: 200,
        message: "Estudiante agregado exitosamente",
        data: JSON.parse(localStorage.getItem('students') || '')
      };
    }
  }
  
  return returnData;
}

  // Eliminar un estudiante según su índice
  deleteStudent(index: number) {
    let studentList = JSON.parse(localStorage.getItem('students') || '');

    studentList.splice(index, 1);

    localStorage.setItem('students', JSON.stringify(studentList));

    let returnData = {
      code: 200,
      message: "Estudiante eliminado exitosamente",
      data: JSON.parse(localStorage.getItem('students') || '')
    };

    return returnData;
  }

  // Obtener los detalles de un estudiante según su índice
  getStudentDetails(index: number) {
    let studentList = JSON.parse(localStorage.getItem('students') || '');

    let returnData = {
      code: 200,
      message: "Detalles del estudiante obtenidos",
      studentData: studentList[index]
    };

    return returnData;
  }

  // Generar un ID aleatorio
  generateRandomID() {
    var x = Math.floor((Math.random() * Math.random() * 9999));
    return x;
  }
}


