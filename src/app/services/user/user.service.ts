/**
 * Creado por: Sangwin Gawande (https://sangw.in)
 */

import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {

	constructor() { }

	// Realiza el inicio de sesión de un usuario
	doLogin(data: any) {
		if (data.email == "miguel@gmail.com" && data.password == "miguel123") {
			return {
				code: 200,
				message: "Inicio de sesión exitoso",
				data: data
			};
		} else if (data.email == "jose@gmail.com" && data.password == "jose123") {
			return {
				code: 200,
				message: "Inicio de sesión exitoso",
				data: data
			};
		} else if (data.email == "paola@gmail.com" && data.password == "paola123") {
			return {
				code: 200,
				message: "Inicio de sesión exitoso",
				data: data
			};
		} else {
			return {
				code: 503,
				message: "Credenciales inválidas",
				data: null
			};
		}
	}

	// Método que podría utilizarse para el registro de usuarios, aunque está comentado
	// doRegister(data) {
	// 	return this.http.post('user-add.php', data);	
	// }
}
